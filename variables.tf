variable "project" {
  default = "project_foo"
}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "cidr" {
  default = "10.0.0.0/16"
}

variable "gcp_credentials" {
  default = "~/foo_creds.json"
}

variable "gcs_bucket" {
  default = "foo_bucket"
}